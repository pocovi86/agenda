package ar.com.besysoft.agenda.entity;

public class Empresa {
    private static int idEmpresa = 1;
    private int id;
    private String razonSocial;
    private String cuit;


    public Empresa() {
        this.id = idEmpresa++;
    }

    public Empresa(String razonSocial, String cuit) {
        this.id = idEmpresa++;
        this.razonSocial = razonSocial;
        this.cuit = cuit;

    }

    @Override
    public String toString() {
        return "\nId: " + id +
                "\nRazon Social: " + razonSocial +
                "\nCuit: " + cuit +
                "\n";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getCuit() {
        return cuit;
    }

    public void setCuit(String cuit) {
        this.cuit = cuit;
    }
}
