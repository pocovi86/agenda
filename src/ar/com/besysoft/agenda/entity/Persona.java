package ar.com.besysoft.agenda.entity;


public class Persona{
    private static int idPersona = 1;
    private int id;
    private String nombre;
    private String apellido;
    private String dni;
    private String email;
    private String ciudad;
    private Empresa empresa;

    public Persona() {
        this.id = idPersona++;
    }

    public Persona(int id, String nombre, String apellido, String dni, String email, String ciudad, Empresa empresa) {
        this.id = idPersona++;
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.email = email;
        this.ciudad = ciudad;
        this.empresa = empresa;
    }

    @Override
    public String toString() {
        return "\nId: " + id +
                "\nNombre: " + nombre +
                "\nApellido: " + apellido +
                "\nDNI: " + dni +
                "\nEmail: " + email +
                "\nCuidad: " + ciudad +
                "\nEmpresa: " + empresa.toString() +
                "\n";
    }

    public static int getIdPersona() {
        return idPersona;
    }

    public static void setIdPersona(int idPersona) {
        Persona.idPersona = idPersona;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
}
