package ar.com.besysoft.agenda.service;

import ar.com.besysoft.agenda.entity.Empresa;
import ar.com.besysoft.agenda.entity.Persona;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Agenda{
    private List<Persona> personas;
    private List<Empresa> empresas;

    private static final String EMAIL_PATTERN = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@" +
            "[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$";
    private static final String CUIT_PATTER = "^(\\d{11})$";
    private static final String DNI_PATTER = "^(\\d{8})$";
    private static final Pattern emailPattern = Pattern.compile(EMAIL_PATTERN);
    private static final Pattern cuitPattern = Pattern.compile(CUIT_PATTER);
    private static final Pattern dniPattern = Pattern.compile(DNI_PATTER);

    public Agenda(){
        personas = new ArrayList<>();
        empresas = new ArrayList<>();
    }

    //***** Empresa *****
    public void crearEmpresa(Empresa empresa) throws Exception{
        if (existEmpresa(empresa.getCuit())){
            throw new Exception("Ya existe una Empresa registrada con el cuit" + empresa.getCuit().trim());
        }
        empresas.add(empresa);
        System.out.println("\nLa siguiente Empresa fue creada: " + "\n" + empresa.toString());
    }

    public boolean existEmpresa(String cuit){
        return empresas.stream().map(Empresa ::getCuit).anyMatch(s -> s.equals(cuit.trim()));
    }

    public void listarEmpresa(){
        if (empresas.isEmpty()){
            System.out.println("\nNo hay empresas registradas");
        } else {
            System.out.println("***** Empresas Registradas *****");
            empresas.forEach(empresa -> System.out.println(empresa.toString()));
        }
    }

    public Empresa buscarEmpresa(String razonSocial) throws Exception{
        for (Empresa empresa: empresas) {
            if (empresa.getRazonSocial().equalsIgnoreCase(razonSocial)){
                return empresa;
            }
        }
        throw new Exception("No se encuentra la empresa con el nombre: " + razonSocial);
    }

    public String validarCuit(String cuit) throws Exception{
        cuit = cuit.trim();
        if (!cuitPattern.matcher(cuit).matches()){
            throw new Exception("Valor ingresado no valido, deben ser 11 digitos");
        }
        return cuit;
    }

    //***** Persona *****
    public void validarListaEmpresa() throws Exception{
        if (empresas.isEmpty()){
            throw new Exception("No hay registro de empresas en la agenda, debe registrar una empresa primero");
        }
    }

    public String validarDni(String dni) throws Exception{
        dni = dni.trim();
        if (!dniPattern.matcher(dni).matches()){
            throw new Exception("Valor ingresado no valido, deben ser 8 digitos");
        }
        return dni;
    }

    public void crearPersona(Persona persona) throws Exception{
        if (empresas.isEmpty()){
            throw new Exception("Para crear una persona debe dar de alta a una empresa");
        }
        if (existePersona(persona.getDni())){
            throw new Exception("Ya existe una persona registrada con el dni: " + persona.getDni());
        }
        personas.add(persona);
        System.out.println("\nLa personas se registro de forma exitosa !");
    }

    public boolean existePersona(String dni){
        return personas.stream().map(Persona::getDni).anyMatch(dniPersona -> dniPersona.equals(dni));
    }

    public void listarPersona(){
        if (personas.isEmpty()){
            System.out.println("\nNo hay personas registradas");
        } else {
            System.out.println("***** Personas Registradas *****");
            personas.forEach(persona -> System.out.println(persona.toString()));
        }

    }

    public void buscarPersona(String filtro){
        List<Persona> personas = this.personas.stream().filter(persona ->
                persona.getNombre().toUpperCase().contains(filtro.toUpperCase()) || persona.getCiudad().toUpperCase().contains(filtro.toUpperCase()))
                .collect(Collectors.toList());

        if (personas.isEmpty()){
            System.out.println("No se encuentro el registro: " + filtro );
        }else {
            System.out.println("Personas registradas con el filtro " + filtro);
            personas.forEach(persona -> System.out.println(persona.toString()));
        }
    }

    public String validarEmail(String email) throws Exception{
        if (email == null || !emailPattern.matcher(email).matches()) {
                throw new Exception("Email invalido");
            }
        return email;
    }
}
