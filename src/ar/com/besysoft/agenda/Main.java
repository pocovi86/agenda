package ar.com.besysoft.agenda;

import ar.com.besysoft.agenda.entity.Empresa;
import ar.com.besysoft.agenda.service.Agenda;
import ar.com.besysoft.agenda.entity.Persona;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    private static Agenda agenda = new Agenda();
    private static final Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        boolean salir = false;

        while (!salir) {
            System.out.println("******************Menú******************");
            System.out.println("1. Registrar una empresa en la agenda");
            System.out.println("2. Listar empresas registradas en la agenda");
            System.out.println("3. Registrar una persona en la agenda");
            System.out.println("4. Listar personas registradas en la agenda");
            System.out.println("5. Filtrar una persona por nombre o cuidad");
            System.out.println("6. Exit");

            try {
                System.out.println("Elija una opcion");
                int opcion = sc.nextInt();

                switch (opcion) {
                    case 1:
                        altaEmpresa();
                        break;
                    case 2:
                        agenda.listarEmpresa();
                        System.out.println("");
                        break;
                    case 3:
                        altaPersona();
                        break;
                    case 4:
                            agenda.listarPersona();
                            System.out.println("");
                        break;
                    case 5:
                        try {
                            System.out.println("\nIngrese el nombre o cuidad: ");
                            String nombre = sc.next();

                            agenda.buscarPersona(nombre);
                            System.out.println("");

                        } catch (Exception ex){
                            System.out.println("Error!!! " + ex.getMessage());
                        }
                        break;
                    case 6:
                        salir = true;
                        System.out.println("Esta es la opcion 6");
                        break;
                        default:
                        System.out.println("Las opciones son entre 1 y 6");
                }
            } catch (InputMismatchException e) {
                System.out.println("Ingrese una opción");
                sc.next();
            }
        }
    }

    public static void altaEmpresa(){
        try {
            Empresa empresa = new Empresa();
            System.out.println("\nIngrese Razon Social de la empresa");
            empresa.setRazonSocial(sc.next());

            System.out.println("\nIngrese cuit de la empresa");
            empresa.setCuit(agenda.validarCuit(sc.next()));

            agenda.crearEmpresa(empresa);

        } catch (Exception ex){
            System.out.println("Error. " + ex.getMessage());
        }
    }

    public static void altaPersona(){
        try {
            agenda.validarListaEmpresa();
            Persona persona = new Persona();
            System.out.println("\nIngrese su nombre: ");
            persona.setNombre(sc.next());

            System.out.println("\nIngrese su apellido: ");
            persona.setApellido(sc.next());

            System.out.println("\nIngrese DNI");
            persona.setDni(agenda.validarDni(sc.next()));

            System.out.println("\nIngrese su email: ");
            persona.setEmail(agenda.validarEmail(sc.next()));

            System.out.println("\nIngrese cuidad: ");
            persona.setCiudad(sc.next());

            System.out.println("");
            System.out.println("Seleccione una empresa del listado");
            agenda.listarEmpresa();
            System.out.println("***********************************");
            System.out.println("");
            System.out.println("Seleccione una empresa por el nombre: ");
            persona.setEmpresa(agenda.buscarEmpresa(sc.next()));

            agenda.crearPersona(persona);
            System.out.println("");


        } catch (Exception ex){
            System.out.println("Error!!! " + ex.getMessage());
        }
    }
}
